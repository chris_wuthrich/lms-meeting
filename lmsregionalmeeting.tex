\documentclass{article}
\usepackage[a4paper,headheight=3ex,body={14cm,22cm}]{geometry}
\usepackage{mathtools, amssymb, amsfonts, booktabs, tcolorbox, url}
\usepackage[british]{babel}
%\usepackage[all]{xy}
\usepackage[textsize=smallsize, textwidth=\marginparwidth]{todonotes}
\usepackage[utf8]{inputenc}

\newcommand{\QQ}{\mathbb{Q}}
\newcommand{\ZZ}{\mathbb{Z}}
\newcommand{\FF}{\mathbb{F}}
\newcommand{\CC}{\mathbb{C}}

\DeclareMathOperator{\ord}{ord}

\DeclareFontFamily{U}{wncy}{}
\DeclareFontShape{U}{wncy}{m}{n}{<->wncyr10}{}
\DeclareSymbolFont{mcy}{U}{wncy}{m}{n}
\DeclareMathSymbol{\Sha}{\mathord}{mcy}{"58}


\newtcolorbox{mybox}[1]{colback=red!5!white,colframe=red!75!black,fonttitle=\bfseries,title=#1}

\begin{document}

\title{Proposal for a LMS regional meeting}
\author{Christian Wuthrich}
\date{}
\maketitle

\begin{description}
  \item[Date:]  September 2019  %\todo[inline]{Unless they get back to me.} %[inline,inlinewidth=2cm, noinlinepar]
  \item[Venue:] University of Nottingham
  \item[Organiser:] Sven Gnutzmann, Fredrik Strömberg and Christian Wuthrich
\end{description}
%
% \begin{mybox}{Instructions}
% Proposals should contain details of:
% \begin{itemize}
% \item proposed topic for the workshop.
% \item the scientific programme, including its accessibility.
% \item speakers for the Regional Society Meeting. The list should include speakers’ names,
% brief addresses (i.e. institution, town and country) and areas of expertise. Please
% indicate any who have provisionally accepted an invitation to attend and the gender of
% all speakers. If no female speakers have been invited, you must give reasons.
% dates.
% \item an outline budget.
% \item details of additional funding, including any departmental support in kind.
% \item compliance with the Society’s Women in Mathematics policy.
% \end{itemize}
% %Should be submitted to the Regional Representative by 15 September of year x – 1.
% \end{mybox}

\section*{LMS Midlands Regional Meeting: Zeta functions in number theory and mathematical physics}
The theme of the regional meeting will be broadly around zeta functions.
From their discovery by Euler, through the fundamental work of Riemann and Kummer, zeta functions have played a crucial role in many areas of mathematics.
It is a meeting point of analytic number theory, algebraic number theory and mathematical physics among many others.
At the meeting there will be three talks, all accessible to a wider audience in these three areas.
The talks will be followed by a wine reception and dinner at the local restaurant ``The Frustrated Chef''.

Unlike the following workshop, the topic of the LMS regional meeting is proposed to have a large perimeter.
This is deliberate to make these talks also available for a wider audience of LMS members and local mathematicians at Nottingham.

The first speaker is \textbf{Ramdorai Sujatha}.
Currently she is professor at the University of British Columbia in Canada.
Her work is in Iwasawa theory which studies values of zeta functions in algebraic number theory, linking them to class numbers and $p$-adic analysis.
In 2006 she received the ICTP Ramanujan Prize.
She has co-authored the book ``Cyclotomic Fields and Zeta Values''~\cite{coates_sujatha} and has published extensively on commutative and non-commutative Iwasawa theory for general Selmer groups.
She is a very inspirational speaker.

The second speaker is \textbf{Nina Snaith} who holds a position as reader at Bristol University.
She is a leading expert in studying the connection between random matrix theory and analytical number theory.
Together with Jon Keating (FRS) she established links between number theoretical functions (such as the Riemann zeta function and $L$-functions) and characteristic polynomials  of random matrices.
Her work is also linked to quantum chaos in mathematical physics.
Moreover she is an excellent speaker who can capture a general audience just as well as an audience of fellow experts.

The third speaker, \textbf{Mark Pollicott}, is a world leading expert in ergodic theory and dynamical systems, and in particular, in the thermodynamical formalism.
Within this field he has made important contributions to the theory of dynamical zeta functions and applications to other areas, such as geometry, number theory and analysis.
Mark is currently a professor at Warwick university and has previously held a professorship in Manchester and a number of visiting and permanent positions.
Amongst his many awards, he has received a Royal Society University Research Fellowship, two Royal Society Leverhulme Trust Senior Research Fellowships, a E.U. Marie Curie Chair and he currently holds an EPSRC Leadership Fellowship.


%
% \begin{mybox}{Instructions}
%   The LMS Programme Committee wishes to stress that lectures at Society meetings need
% to appeal to the general membership of the Society. This means that context and
% motivation are important, that the broad sweep of the ideas are favoured over technical
% details, and that visual materials are attractive and can be understood from the back of
% the room. The advice regarding the level of technical detail is: ``aim low and then halve
% it''. Further: titles of talks and published abstracts need to convey that the talk will appeal
% to a general LMS membership, and therefore need to be expressed in words
% understandable to a final year undergraduate. Society meetings are not specialist
% workshop talks, and must be, and be seen to be, accessible and interesting. A lecture at a
% Society meeting is a great opportunity to convey the excitement and vibrancy of a
% speaker’s research area to a wider mathematical audience.
% \end{mybox}


\section*{Workshop: Elliptic curves and their friends and relatives}

The topic of the workshop after the LMS regional meeting is proposed to be on a narrower field.
Before going into more technical details, we give a vague descriptions of this field of research.
The workshop will be centred around number theoretic questions concerning elliptic curves.
These curves live on the fertile ground on which to study deep conjectural connections between their arithmetic and zeta-functions (or better $L$-functions in this context) like the Birch and Swinnerton-Dyer conjecture, which is among 1000000\textdollar\ Millennium problems~\cite{bsd} by the Clay Mathematics Institute.
They are among the easiest objects which exhibit already most of the complicated phenomena of these connections.
Elliptic curves are sufficiently amenable to explicit computations to test and discover properties.
Likewise there is also a great amount of progress on these conjectures mainly through studying the modular forms associated to the elliptic curves by the work of Taylor and Wiles.
We could list here the theorem by Gross--Zagier and Kolyvagin, their converse by Zhang and Skinner, Kato's Euler system and the work of Skinner--Urban among many others.
A good overview of these developments can be found in~\cite{zhang_survey}.

The methods producing these results have now been taken further afield to areas that are direct generalisations.
This workshop should bring together researchers working on arithmetic questions on elliptic curves and more general objects like modular and automorphic forms, higher genus curves, abelian varieties and Galois representations.
One focus point will be the various generalisations of the Birch and Swinnerton-Dyer conjecture.

Now to the more precise mathematical description.
Let $k$ be a number field and let $E$ be an elliptic curve defined over $k$.
The zeta function of $E$ splits into two factors that are shifted Riemann zeta function and the $L$-function $L(E,s)$ of $E$.
This $L$-function can be defined as the Euler product $L(E/k,s) = \prod_v P_v(q_v^{-s})^{-1}$ where $v$ runs through the prime ideals of $k$, the local polynomial $P_v$ is usual quadratic and $q_v$ denotes the number of elements in the residue field $\FF_v$ of $v$.
%If $v$ has good reduction then $P_v(X)= 1 - a_v\, X + q_v\,X^2$ with $a_v = q_v+1-\# E(\FF_v)$, otherwise it is a polynomial of smaller degree depending on the type of the reduction of $E$ at $v$.
While this Dirichlet series converges for $\operatorname{Re}(s)>\tfrac{3}{2}$, it is conjectured to have an analytic continuation to all $s\in \CC$.
The conjecture of Birch and Swinnerton-Dyer (BSD) states firstly that the order of vanishing $\ord_{s=1} L(E/k,s)$ should be equal to the rank of the Mordell-Weil group $E(k)$ of rational points on $E$ defined over $k$.
If $k=\QQ$, there are very deep results that show this part of the conjecture when the order of vanishing is at most $1$.
A second part of the conjecture describes the leading term $L^*(E,1)$ of $L(E,s)$ when expanded at $s=1$ in terms of arithmetic information of $E$.
In particular this formula contains the order of the mysterious Tate-Shafarevich group $\Sha(E/k)$, which is not known to be finite, as well as terms called Tamagawa numbers for primes of bad reduction and a transcendental regulator.
Again many statements are known about the correctness of this formula for $k=\QQ$ and $L(E,1)\neq 0$ or $L'(E,1)\neq 0$.

Suppose now $K/k$ is a finite Galois extension with group $G$.
The BSD conjecture for $E/K$ can be refined as it splits into conjectures about the twisted $L$-function $L(E,\rho,s)$ for $\rho$ running through the irreducible complex representations of $G$.
We expect that the order of vanishing of this twisted $L$-function is the dimension of the $\rho$-part of the representation $E(K)\otimes \CC$ of $G$.
Recently, some cases of this generalisation of the BSD conjectures were proven; see for instance~\cite{bdr}.
There are conjectures about the leading term, too; they are however far more involved, especially when looking at the primes $p$ dividing the order of $G$.

Iwasawa theory takes this situation to its limit: If $K/k$ is a Galois extension whose group $\Gamma$ is isomorphic to the additive group of $p$-adic integers $\ZZ_p$, then there are very interesting new objects that come into play.
On the analytic side, the values of $L(E,\chi,1)$ as $\chi$ runs through finite characters of $\Gamma$ can be used to define a $p$-adic analogue of the $L$-function whenever we can prove $p$-adic congruences between the values of the twisted $L$-functions.
On the algebraic side the Mordell-Weil group, or more generally the Selmer group, of $E$ for fields intermediate to $K/k$ carry the structure of a module over the completed group ring $\Lambda$ of $\Gamma$.
The main conjecture of Iwasawa theory predicts a precise link between the algebraic and the analytic side.
While the main conjecture is now known in many cases when $k=\QQ$, it remains mysterious in more general situations.
Yet any progress on this, for instance through the construction of new Euler systems, provides also new results about the original question.

As mentioned above, the conjectural picture predicted by the BSD conjecture and the Iwasawa main conjecture has been generalised to lots of interesting objects like modular forms, higher genus curves and general Galois representations.
See~\cite{bloch_kato}.
The variant involving an extra group action by $G$ is called the equivariant Tamagawa number conjecture~\cite{burns_flach}.

The aim of the workshop is to bring together experts from the UK and abroad as well as graduate students and early career researchers who work on elliptic curves or some of these areas close-by.
%We hope this will create an stimulating exchange of ideas and methods.

\section*{Principal speakers for workshop}


Sarah Zerbes (University College London) and Tim Dokchitser (Bristol) have agreed to act as a scientific committee for the workshop.

The following principal speakers are expected to be included in the prospective programme of the workshop.
Some, but not all, have already been contacted and they have confirmed to be interested in speaking at the workshop.

Yukako Kezuka (Regensburg),
Werner Bley (München),
Kazim Büyükboduk (University College Dublin),
Vladimir Dokchitser (King's College London),
John Cremona (Warwick),
Mahesh Kakde (King's College London),
Henri Johnston (Exeter),
Sarah Zerbes (University College London).

There are 4 female speakers in these 11 speakers (including the three LMS regional meeting speakers).
Since we expect that some of the above may not be available, we add here a list of further potential speakers:

Alex Bartel (Glasgow),
Alexander Betts (King's College London),
David Burns (King's College London),
John Coates (Cambridge),
Dimitar Jetchev (École polytechnique de Lausanne),
Jaclyn Lang (Max Planck Bonn),
David Loeffler (Warwick),
Marusia Rebolledo (Clermont-Ferrand),
Victor Rotgers (Universitat Politècnica de Ca\-ta\-lun\-ya),
Jack Thorne (Cambridge),
Jeanine van Order (Bielefeld).


\section*{Budget for meeting}

\subsubsection*{Accommodation:}

Accommodation for all 8~speakers of the workshop for 3~nights : \\
$\qquad 8\times 3 \times \mathsterling 85 = \mathsterling 2295$

\noindent Accommodation for the two speakers of the regional meeting for one night : \\
$\quad 2 \times \mathsterling 85 = \mathsterling 170$

\subsubsection*{Travel:}
\begin{itemize}
\item 1 person from Canada : $1 \times \mathsterling 800 = \mathsterling 800$

\item 3 persons from Europe : $3 \times \mathsterling 300 = \mathsterling 900$

\item 5 persons from the UK : $5 \times \mathsterling 100 = \mathsterling 500$

\item Support for postgraduate students and LMS members : $\mathsterling 500$

\end{itemize}

\subsubsection*{Other:}
\begin{itemize}
\item LMS Dinner for 11~speakers : \\
$\qquad 11\times \mathsterling 25 = \mathsterling 275$
\item Wine reception: \\
$\qquad \mathsterling 100$
\item Coffee breaks : \\
$\qquad \mathsterling 100$
\end{itemize}

\subsubsection*{Total:}
$\qquad\mathsterling 5640$

\bibliographystyle{amsplain}
\bibliography{lmsmeeting}


\end{document}